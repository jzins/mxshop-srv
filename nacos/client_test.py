import nacos

SERVER_ADDRESSES = "192.168.10.130:8848"
# 这里是namespace的id
NAMESPACE = "d35c6a37-2a22-465d-944c-ef3a58c9bee0"

client = nacos.NacosClient(SERVER_ADDRESSES, namespace=NAMESPACE, username="nacos", password="nacos")
data_id = "user-srv.json"
group = "dev"
print(type(client.get_config(data_id, group)))  # 返回的是字符串
import json

json_data = json.loads(client.get_config(data_id, group))
print(json_data)


def test_cb(args):
    print("配置文件产生变化")
    print(args)


if __name__ == '__main__':
    client.add_config_watcher(data_id, group, test_cb)#这个必须放在main里面完成
    import time

    time.sleep(3000)
